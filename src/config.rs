use std::net::{IpAddr, Ipv4Addr};

use ac_server_plugin_lib::{PluginPoint, ServerConfig as ACServerConfig};
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServerConfig {
    pub address: IpAddr,
    pub command_port: u16,
    pub data_port: u16,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ForwardConfig {
    pub address: IpAddr,
    pub command_port: u16,
    pub data_port: u16,
    pub enabled: bool,
}

impl Default for ForwardConfig {
    fn default() -> Self {
        Self {
            address: IpAddr::V4(Ipv4Addr::UNSPECIFIED),
            command_port: 0,
            data_port: 0,
            enabled: false,
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DirectoriesConfig {
    pub server_folder: String,
    pub log_folder: String,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Config {
    pub timeout: u64,
    pub server: ServerConfig,
    pub forwarding: ForwardConfig,
    pub directories: DirectoriesConfig,
}

impl Config {
    pub fn get_server_config(&self) -> ACServerConfig {
        ACServerConfig {
            server: PluginPoint {
                command_port: (self.server.address, self.server.command_port).into(),
                data_port: (self.server.address, self.server.data_port).into(),
            },
            forward: self.forwarding.enabled.then(|| PluginPoint {
                command_port: (self.forwarding.address, self.forwarding.command_port).into(),
                data_port: (self.forwarding.address, self.forwarding.data_port).into(),
            }),
        }
    }
}
