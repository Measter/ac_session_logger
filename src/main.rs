use std::{fs::File, io::BufReader};

use ac_server_plugin_lib::Server;
use anyhow::{anyhow, Context, Result};
use log::{error, info, trace};

mod config;
mod handler;
mod logger;

use handler::*;

use crate::logger::EventLogger;

static CONFIG_FILE: &str = "config.json";

fn run() -> Result<()> {
    info!("Session Result Logger Prototype");
    info!("Loading config...");
    let config =
        File::open(CONFIG_FILE).with_context(|| anyhow!("Unable to find \"{}\"", CONFIG_FILE))?;
    let config: config::Config = serde_json::from_reader(BufReader::new(config))
        .with_context(|| anyhow!("Unable parse config"))?;

    trace!("{:#?}", config);
    info!("Config loaded.");

    crossbeam_utils::thread::scope(|s| -> Result<()> {
        let (sender, receiver) = crossbeam_channel::unbounded::<LogEvent>();

        // Create logging thread here.
        info!("Creating event logger");
        let logger = EventLogger::new(&config, receiver);

        s.spawn(move |_| {
            logger.run();
        });

        info!("Initializing server...");
        let mut server = Server::new(config.get_server_config());

        info!("Creating driver database");
        let driver_db = server.get_driver_db();

        info!("Creating logging handler");
        server.add_event_handler(LogHandler::new(driver_db, sender));

        // Need to format it here because anyhow wants it Sync, but crossbeam doesn't return a Sync.
        // It's annoying.
        server.run().map_err(|e| anyhow!("{}", e))?;

        Ok(())
    })
    .expect("Unable to catch thread panic!")?;

    Ok(())
}

fn main() {
    log4rs::init_file("logging.yaml", Default::default()).expect("Unable to initialize logger");

    if let Err(e) = run() {
        error!("An unhandled error ocurred:");

        for cause in e.chain() {
            error!(" - {}", cause);
        }

        std::process::exit(1);
    }
}
