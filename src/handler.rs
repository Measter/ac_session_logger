use std::time::Duration;

use ac_server_plugin_lib::{
    Commander, DriverDB, EventHandler, LapCompletedInfo, ServerError, SessionInfo, SessionLength,
    SessionType,
};
use chrono::{DateTime, Utc};
use crossbeam_channel::Sender;
use log::{debug, error, trace};

pub struct LogHandler {
    drivers: DriverDB,
    event_sender: Sender<LogEvent>,
    known_session: bool,
}

impl LogHandler {
    pub fn new(drivers: DriverDB, event_sender: Sender<LogEvent>) -> Self {
        Self {
            drivers,
            event_sender,
            known_session: false,
        }
    }
}

impl EventHandler for LogHandler {
    fn name(&self) -> &str {
        "Session Result Logger"
    }

    fn on_protocol_version(&mut self, _: &mut Commander<'_>, _: u8) -> Result<(), ServerError> {
        if let Err(e) = self.event_sender.send(LogEvent::FullReset) {
            error!("Failed to send full reset event");
            error!("{:#?}", e);
        };

        self.known_session = false;

        Ok(())
    }

    fn on_new_session(
        &mut self,
        cmdr: &mut Commander<'_>,
        info: &SessionInfo,
    ) -> Result<(), ServerError> {
        self.known_session = false;
        self.on_session_info(cmdr, info)
    }

    fn on_session_info(
        &mut self,
        _: &mut Commander<'_>,
        info: &SessionInfo,
    ) -> Result<(), ServerError> {
        if self.known_session {
            return Ok(());
        }

        let event = LogEvent::NewSession(NewSession {
            timestamp: Utc::now(),
            track_name: info.track_name.clone(),
            track_config: info.track_layout.clone(),
            session_type: info.session_type,
            session_duration: info.session_length,
        });

        debug!("Sending new session event");
        trace!("{:#?}", event);

        if let Err(e) = self.event_sender.send(event) {
            error!("Failed to send new session event");
            error!("{:#?}", e);
        }

        Ok(())
    }

    fn on_lap_completed(
        &mut self,
        _: &mut Commander<'_>,
        info: &LapCompletedInfo,
    ) -> Result<(), ServerError> {
        let driver = match self.drivers.get_driver_by_id(info.car_id) {
            Some(driver) => driver,
            None => {
                error!("Driver {} didn't exist in database", info.car_id);
                return Ok(());
            }
        };

        let event = LogEvent::NewLap(NewLap {
            driver_name: driver.name.clone(),
            driver_guid: driver.guid.clone(),
            driver_team: driver.team.clone(),
            car_id: info.car_id,
            timestamp: Utc::now(),
            lap_time: info.laptime,
            cuts: info.cuts,
        });

        debug!("Sending lap completed event");
        trace!("{:#?}", event);

        if let Err(e) = self.event_sender.send(event) {
            error!("Failed to send new lap event");
            error!("{:#?}", e);
            return Ok(());
        }

        let mut leaderboard = Vec::new();

        for lbe in &info.leaderboard {
            let guid = self
                .drivers
                .get_driver_by_id(lbe.car_id)
                .map(|d| d.guid.clone());

            leaderboard.push(LeaderboardEntry {
                driver_guid: guid,
                car_id: lbe.car_id,
            });
        }

        let event = LogEvent::Leaderboard(leaderboard);

        debug!("Sending new leaderboard");
        trace!("{:#?}", event);

        if let Err(e) = self.event_sender.send(event) {
            error!("Failed to send leaderboard event");
            error!("{:#?}", e);
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct LeaderboardEntry {
    pub driver_guid: Option<String>,
    pub car_id: u8,
}

#[derive(Debug)]
pub struct NewSession {
    pub timestamp: DateTime<Utc>,
    pub track_name: String,
    pub track_config: String,
    pub session_type: SessionType,
    pub session_duration: SessionLength,
}

#[derive(Debug)]
pub struct NewLap {
    pub driver_name: String,
    pub driver_guid: String,
    pub driver_team: String,
    pub car_id: u8,
    pub timestamp: DateTime<Utc>,
    pub lap_time: Duration,
    pub cuts: u8,
}

#[derive(Debug)]
pub enum LogEvent {
    FullReset,
    NewSession(NewSession),
    NewLap(NewLap),
    Leaderboard(Vec<LeaderboardEntry>),
}
