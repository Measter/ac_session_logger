use std::{
    collections::HashMap,
    fs::File,
    io::BufWriter,
    path::{Path, PathBuf},
    time::Duration,
};

use ac_server_plugin_lib::{SessionLength, SessionType};
use anyhow::{anyhow, Context, Result};
use chrono::{DateTime, Utc};
use crossbeam_channel::Receiver;
use ini::{Ini, Properties};
use log::{debug, error, info, trace, warn};
use serde::Serialize;

use crate::{
    config::Config,
    handler::{LeaderboardEntry, LogEvent, NewLap, NewSession},
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
struct OutputCarDriver<'a> {
    name: &'a str,
    team: &'a str,
    nation: &'a str,
    guid: &'a str,
    guids_list: [&'a str; 1],
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
struct OutputCar<'a> {
    car_id: u8,
    model: &'a str,
    skin: &'a str,
    #[serde(rename = "BallastKG")]
    ballast: u8,
    restrictor: u8,
    driver: OutputCarDriver<'a>,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
struct OutputSessionResult<'a> {
    driver_name: &'a str,
    driver_guid: &'a str,
    car_id: u8,
    car_model: &'a str,
    best_lap: u32,
    total_time: u32,
    #[serde(rename = "BallastKG")]
    ballast: u8,
    restrictor: u8,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
struct OutputLap<'a> {
    driver_name: &'a str,
    driver_guid: &'a str,
    car_id: u8,
    car_model: &'a str,
    timestamp: u64,
    laptime: u32,
    sectors: [u32; 3],
    cuts: u8,
    #[serde(rename = "BallastKG")]
    ballast: u8,
    tyre: &'a str,
    restrictor: u8,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
struct OutputSession<'a> {
    track_name: &'a str,
    track_config: &'a str,
    session_type: &'static str,
    duration_secs: u32,
    race_laps: u32,
    cars: Vec<OutputCar<'a>>,
    result: Vec<OutputSessionResult<'a>>,
    laps: Vec<OutputLap<'a>>,
    events: Option<Vec<()>>, // Never used, but needed for the output.
}

#[derive(Debug)]
struct Car {
    id: u8,
    model: String,
    skin: String,
    ballast: u8,
    restrictor: u8,
}

impl Car {
    fn parse(section: &str, keys: &Properties) -> Result<Car> {
        let id: u8 = section
            .strip_prefix("CAR_")
            .or_else(|| section.strip_prefix("car_"))
            .ok_or_else(|| anyhow!("Invalid section name: {}", section))?
            .parse()
            .with_context(|| anyhow!("Failed to parse car ID in section {}", section))?;

        let get_key = |name: &str| {
            keys.get(name)
                .ok_or_else(|| anyhow!("Key {} not found in entry {}", name, section))
        };

        Ok(Car {
            id,
            model: get_key("MODEL")?.to_owned(),
            skin: get_key("SKIN")?.to_owned(),

            ballast: get_key("BALLAST")?
                .parse()
                .with_context(|| anyhow!("Failed to parse ballast for car {}", section))?,
            restrictor: get_key("RESTRICTOR")?
                .parse()
                .with_context(|| anyhow!("Failed to parse restrictor for car {}", section))?,
        })
    }
}

struct Lap {
    car_id: u8,
    driver_guid: String,
    timestamp: DateTime<Utc>,
    lap_time: Duration,
    cuts: u8,
}
struct Driver {
    name: String,
    guid: String,
    team: String,
}

struct Session {
    session_start: DateTime<Utc>,
    track_name: String,
    track_config: String,
    session_type: SessionType,
    session_duration: SessionLength,
    cars: Vec<Car>,
    laps: Vec<Lap>,
    drivers: HashMap<String, Driver>,
    leaderboard: Vec<LeaderboardEntry>,
    entry_list_path: PathBuf,
}

impl Session {
    fn clear(&mut self) {
        self.session_start = Utc::now();
        self.track_config.clear();
        self.track_name.clear();
        self.session_type = SessionType::None;
        self.session_duration = SessionLength::Time(Duration::from_secs(0));
        self.cars.clear();
        self.laps.clear();
        self.drivers.clear();
    }

    fn load_entry_list(&mut self) {
        debug!("Loading entry list from {}", self.entry_list_path.display());

        let file = match Ini::load_from_file(&self.entry_list_path) {
            Ok(f) => f,
            Err(e) => {
                error!("Failed to open entry list file: {}", e);
                return;
            }
        };

        for (section, keys) in file.iter().filter_map(|(a, b)| a.map(|a| (a, b))) {
            let car = match Car::parse(section, keys) {
                Ok(c) => c,
                Err(e) => {
                    error!("Failed to parse car {}: {:?}", section, e);
                    continue;
                }
            };

            self.cars.push(car);
        }

        debug!("Entry list loaded, {} cars", self.cars.len());
        trace!("{:#?}", self.cars);
    }

    fn new(entry_list_path: PathBuf) -> Self {
        Self {
            session_start: Utc::now(),
            track_config: String::new(),
            track_name: String::new(),
            session_type: SessionType::None,
            session_duration: SessionLength::Time(Duration::from_secs(0)),
            cars: Vec::new(),
            laps: Vec::new(),
            drivers: HashMap::new(),
            entry_list_path,
            leaderboard: Vec::new(),
        }
    }

    fn new_session(&mut self, new_session: NewSession) {
        self.session_start = new_session.timestamp;
        self.session_duration = new_session.session_duration;
        self.session_type = new_session.session_type;
        self.track_name.replace_range(.., &new_session.track_name);
        self.track_config
            .replace_range(.., &new_session.track_config);
        self.laps.clear();
    }

    fn add_new_lap(&mut self, lap: NewLap) {
        if !self.drivers.contains_key(&lap.driver_guid) {
            self.drivers.insert(
                lap.driver_guid.clone(),
                Driver {
                    name: lap.driver_name,
                    guid: lap.driver_guid.clone(),
                    team: lap.driver_team,
                },
            );
        }

        self.laps.push(Lap {
            car_id: lap.car_id,
            driver_guid: lap.driver_guid,
            timestamp: lap.timestamp,
            lap_time: lap.lap_time,
            cuts: lap.cuts,
        });
    }
}

pub struct EventLogger {
    receiver: Receiver<LogEvent>,
    log_directory: String,
    session_info: Session,
    timeout: u64,
}

impl EventLogger {
    pub fn new(config: &Config, receiver: Receiver<LogEvent>) -> Self {
        let entry_list_path =
            Path::new(&config.directories.server_folder).join("cfg/entry_list.ini");
        debug!("Entry list path: {}", entry_list_path.display());

        Self {
            receiver,
            log_directory: config.directories.log_folder.clone(),
            session_info: Session::new(entry_list_path),
            timeout: config.timeout,
        }
    }

    fn write_state(&self) {
        if self.session_info.laps.is_empty() {
            info!("No laps done, no output written");
            return;
        }

        info!("Bulding output...");

        let mut cars: Vec<_> = self
            .session_info
            .cars
            .iter()
            .map(|car| {
                OutputCar {
                    car_id: car.id,
                    model: &car.model,
                    skin: &car.skin,
                    ballast: car.ballast,
                    restrictor: car.restrictor,
                    // This is the expected output, so this is what we get...
                    driver: OutputCarDriver {
                        name: "",
                        team: "",
                        nation: "",
                        guid: "",
                        guids_list: [""; 1],
                    },
                }
            })
            .collect();

        let mut fastest_lap = vec![Duration::from_secs(999_999_999); cars.len()];
        let mut total_time = vec![Duration::from_secs(0); cars.len()];
        let mut laps = Vec::new();
        for lap in &self.session_info.laps {
            let car = match self.session_info.cars.get(lap.car_id as usize) {
                Some(c) => c,
                None => {
                    error!(
                        "Unable to find car {} for lap {:?} by driver {}",
                        lap.car_id, lap.lap_time, lap.driver_guid
                    );
                    continue;
                }
            };

            let driver = match self.session_info.drivers.get(&lap.driver_guid) {
                Some(d) => d,

                None => {
                    error!("Unable to find driver with guid \"{}\"", lap.driver_guid);
                    continue;
                }
            };

            if lap.cuts == 0 {
                fastest_lap[lap.car_id as usize] =
                    fastest_lap[lap.car_id as usize].min(lap.lap_time);
            }
            total_time[lap.car_id as usize] += lap.lap_time;

            // Output seems to be the last one in the car.
            let output_car = &mut cars[car.id as usize];
            output_car.driver = OutputCarDriver {
                name: &driver.name,
                team: &driver.team,
                nation: "", // Not in the plugin API
                guid: &driver.guid,
                guids_list: [&driver.guid; 1],
            };

            let timestamp = match (lap.timestamp - self.session_info.session_start).to_std() {
                Ok(ts) => ts,
                Err(_) => {
                    error!(
                        "Timestamp {} for lap {:?} by driver {} was invalid",
                        lap.timestamp, lap.lap_time, lap.driver_guid
                    );
                    Duration::from_secs(0)
                }
            };
            laps.push(OutputLap {
                driver_name: &driver.name,
                driver_guid: &driver.guid,
                car_id: car.id,
                car_model: &car.model,
                timestamp: timestamp.as_millis() as _,
                laptime: lap.lap_time.as_millis() as _,
                sectors: [0; 3], // No sector information, unfortunately
                cuts: lap.cuts,
                ballast: car.ballast,
                tyre: "", // No tyre info, neither
                restrictor: car.restrictor,
            });
        }

        // Needed because the results must have a driver.
        let empty_driver = Driver {
            name: String::new(),
            guid: String::new(),
            team: String::new(),
        };
        let mut results = Vec::new();
        for res in &self.session_info.leaderboard {
            let car = match self.session_info.cars.get(res.car_id as usize) {
                Some(c) => c,
                None => {
                    error!(
                        "Unable to find car {}, driver {}",
                        res.car_id,
                        res.driver_guid.as_deref().unwrap_or("")
                    );
                    continue;
                }
            };

            let driver = if let Some(driver_guid) = &res.driver_guid {
                match self.session_info.drivers.get(driver_guid) {
                    Some(d) => d,
                    None => {
                        error!("Unable to find driver with guid {}", driver_guid);
                        continue;
                    }
                }
            } else {
                &empty_driver
            };

            results.push(OutputSessionResult {
                driver_name: &driver.name,
                driver_guid: &driver.guid,
                car_id: car.id,
                car_model: &car.model,
                best_lap: fastest_lap[car.id as usize].as_millis() as _,
                total_time: total_time[car.id as usize].as_millis() as _,
                ballast: car.ballast,
                restrictor: car.restrictor,
            });
        }

        let session_type = match self.session_info.session_type {
            SessionType::Practice => "PRACTICE",
            SessionType::Qualifying => "QUALIFY",
            SessionType::Race => "RACE",
            SessionType::None => "NONE",
        };

        let (duration_secs, race_laps) = match self.session_info.session_duration {
            SessionLength::Time(t) => (t.as_secs() as _, 0),
            SessionLength::Laps(l) => (0, l as _),
        };

        let session = OutputSession {
            track_name: &self.session_info.track_name,
            track_config: &self.session_info.track_config,
            session_type,
            duration_secs,
            race_laps,
            cars,
            result: results,
            laps,
            events: None,
        };

        let filename = format!(
            "{}_{}.json",
            self.session_info.session_start.format("%Y_%-m_%-d_%-H_%-M"),
            match self.session_info.session_type {
                SessionType::None => "NONE",
                SessionType::Qualifying => "QUALIFY",
                SessionType::Race => "RACE",
                SessionType::Practice => "PRACTICE",
            }
        );

        let path = Path::new(&self.log_directory).join(&filename);

        info!("Log path: {}", path.display());

        let file = match File::create(path) {
            Ok(f) => f,
            Err(e) => {
                error!("Error opening output file: {:?}", e);
                return;
            }
        };

        info!("Writing log to {}", filename);

        if let Err(e) = serde_json::to_writer_pretty(BufWriter::new(file), &session) {
            error!("Error writing log file: {:?}", e);
        }
    }

    pub fn run(mut self) {
        loop {
            match self
                .receiver
                .recv_timeout(Duration::from_secs(self.timeout))
            {
                Ok(LogEvent::FullReset) => {
                    debug!("Received full reset");
                    self.write_state();
                    self.session_info.clear();
                    self.session_info.load_entry_list();
                }
                Ok(LogEvent::NewSession(session)) => {
                    debug!("Received new session");
                    trace!("{:#?}", session);
                    self.write_state();

                    info!("New session");

                    if self.session_info.cars.is_empty() {
                        warn!("Cars was empty on a new session, suggesting plugin was started after server");
                        warn!("Performing a full reset");
                        self.session_info.clear();
                        self.session_info.load_entry_list();
                    }

                    self.session_info.new_session(session);
                }
                Ok(LogEvent::NewLap(lap_info)) => {
                    info!("Received new lap");
                    trace!("{:#?}", lap_info);

                    if self.session_info.cars.is_empty() {
                        warn!("Cars was empty on a new lap, suggesting plugin was started after server");
                        warn!("Performing a full reset");
                        self.session_info.clear();
                        self.session_info.load_entry_list();
                    }

                    self.session_info.add_new_lap(lap_info);
                    self.write_state();
                }
                Ok(LogEvent::Leaderboard(leaderboard)) => {
                    debug!("Received leaderboard");
                    trace!("{:#?}", leaderboard);

                    if self.session_info.cars.is_empty() {
                        warn!("Cars was empty on a new leaderboard, suggesting plugin was started after server");
                        warn!("Performing a full reset");
                        self.session_info.clear();
                        self.session_info.load_entry_list();
                    }

                    self.session_info.leaderboard = leaderboard;
                    self.write_state();
                }
                Err(crossbeam_channel::RecvTimeoutError::Timeout) => {
                    debug!("Receiver timeout");
                    self.write_state();
                }
                Err(crossbeam_channel::RecvTimeoutError::Disconnected) => {
                    error!("Receiver was disconnected!");
                    return;
                }
            };
        }
    }
}
